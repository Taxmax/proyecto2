from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django import forms
from .models import Cliente,Servicio
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from .forms import RegistroForm, PerfilUsuarioForm
from .serializers import ClienteSerializer
from rest_framework import generics


class API_objects(generics.ListCreateAPIView):
        queryset = Cliente.objects.all()
        serializer_class = ClienteSerializer

class API_objects_details(generics.RetrieveUpdateDestroyAPIView):
        queryset = Cliente.objects.all()
        serializer_class = ClienteSerializer

def listar_clientes(request):
    clientes = Cliente.objects.all()
    return render(request,
        "gestion/listar_clientes.html" , {'clientes' : clientes})

def listar_clientes_full(request):
    clientes = Cliente.objects.all()
    return render(request,
        "gestion/listar_clientes_full.html", {'clientes' : clientes})


def editar_cliente(request, rut):
    instancia= Cliente.objects.get(rut=rut)
    form= PerfilUsuarioForm(instance=instancia)
    if request.method=="POST":
        form= PerfilUsuarioForm(request.POST, instance=instancia)
        if form.is_valid():
            instancia= form.save(commit=False)
            instancia.save()
    return render(request, "gestion/editar_cliente.html",{'form':form})

def borrar_cliente(request, rut):
    instancia= Cliente.objects.get(rut=rut)
    instancia.delete()
    return redirect('gestion:listar_clientes_full')


def Servicio(request):

    return render(request,'gestion/Servicio.html') 

def Main(request):

    return render(request, 'gestion/Main.html')

def Nosotros(request):

    return render(request,'gestion/Nosotros.html')    

def Contacto(request):

    return render(request,'gestion/Contacto.html')      
    
def Usuario_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('gestion:Main'))

def Index(request):
    return render(request, 'gestion/Main.html')    


def Usuario_login(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('gestion:Main'))
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    if request.user.is_superuser:
                        login(request, user)
                        return HttpResponseRedirect(reverse('gestion:admin.html'))
                    else:    
                        login(request, user)
                        return HttpResponseRedirect(reverse('gestion:Main'))
                else:
                    return HttpResponse("Tu cuenta está inactiva    UwU")      
            else: 
                print("username: {} - password: {}".format(username, password))
                return HttpResponse("Datos Inválidos")
        else:
            return render(request, 'gestion/Login.html',{})        


def Registrar(request):
    registrado= False
    if request.method == 'POST':
        user_form = RegistroForm(data=request.POST)
        profile_form = PerfilUsuarioForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()
            registrado = True
        else:       
            print(user_form.errors, profile_form.errors)     
            return HttpResponse("Datos Invalidos")    
    else:
        user_form = RegistroForm()     
        profile_form = PerfilUsuarioForm()

    return render(request, 'gestion/Registro.html',
                     {'user_form': user_form,
                     'profile_form': profile_form,
                     'registrado': registrado})


def post_list(request):
    user = request.user
    if user.has_perm('TaxMaxapp.cliente'):
        posts = Post.objects.filter(
            published_date__lte=timezone.now()).order_by('published_date')
        return render(request, 'gestion/post_list.html', {'posts': posts})
    else:
        return render(request, 'gestion/main.html')  

# ---importamos nuestras estruras ---------------


# Create your views here.

