from django.apps import AppConfig


class TaxmaxappConfig(AppConfig):
    name = 'TaxMaxapp'
