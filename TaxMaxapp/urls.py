from django.conf import settings
from django.urls import path
from . import views
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *
app_name = 'gestion'

#---------- rutas de la API ----------------------------



urlpatterns = [    
    path('api/', views.API_objects.as_view()),
    path('api/<int:pk>/', views.API_objects_details.as_view()),
    path('', views.Main, name='Main'),
    path('Nosotros/', views.Nosotros, name='Nosotros'),
    path('Main/', views.Main, name='Main'),
    path('Servicio/', views.Servicio, name='Servicio'),
    path('Registro/', views.Registrar, name='Registro'),
    path('Login/', views.Usuario_login, name='Login'),
    path('Logout/', views.Usuario_logout, name='Logout'),
    path('Contacto/', views.Contacto, name='Contacto'),
    path('listar_clientes/',views.listar_clientes, name='listar_clientes'),
    path('editar_cliente/', views.editar_cliente, name='editar_cliente'),
    path('editar_cliente/<str:rut>', views.editar_cliente, name='editar_cliente'),
    path('borrar_cliente/', views.borrar_cliente, name='borrar_cliente'),
    path('borrar_cliente/<str:rut>', views.borrar_cliente, name='borrar_cliente'),
    path('listar_clientes_full',views.listar_clientes_full, name='listar_clientes_full'),    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns = format_suffix_patterns(urlpatterns)





















