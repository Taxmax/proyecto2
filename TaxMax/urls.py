"""appCrud URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
# Este es el "directorio" de las rutas generales!!!
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('TaxMaxapp.urls')),
    path('api/', include('TaxMaxapp.urls')),
    path('Nosotros/', include('TaxMaxapp.urls')),
    path('Servicio/', include('TaxMaxapp.urls')),    
    path('Registro/', include('TaxMaxapp.urls')),
    path('Login/', include('TaxMaxapp.urls')),
    path('Logout/', include('TaxMaxapp.urls')),
    path('Contacto/', include('TaxMaxapp.urls')),
    path('Main/', include('TaxMaxapp.urls')),
    path('listar_clientes',include('TaxMaxapp.urls')),
    path('editar_cliente/<str:rut>', include('TaxMaxapp.urls')),
    path('borrar_cliente/<str:rut>', include('TaxMaxapp.urls')),
    path('listar_clientes_full',include('TaxMaxapp.urls')),  
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
